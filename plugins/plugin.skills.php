<?php

require __DIR__ . '/vendor/autoload.php';

use Moserware\Skills\GameInfo;
use Moserware\Skills\Player;
use Moserware\Skills\Rating;
use Moserware\Skills\Team;
use Moserware\Skills\Teams;
use Moserware\Skills\RankSorter;
use Moserware\Skills\SkillCalculator;
use Moserware\Skills\TrueSkill\FactorGraphTrueSkillCalculator;

require_once('includes/elo.php');

Aseco::registerEvent('onSync', 'skills_onSync');
Aseco::registerEvent('onPlayerConnect', 'skills_onPlayerConnect');
Aseco::registerEvent('onNewChallenge', 'skills_onNewChallenge');
Aseco::registerEvent('onBeginRound', 'skills_onBeginRound');
Aseco::registerEvent('onEndRace', 'skills_onEndRace');
Aseco::registerEvent('onEverySecond', 'skills_onEverySecond');
Aseco::registerEvent('onPlayerManialinkPageAnswer', 'skills_onPlayerManialinkPageAnswer');
Aseco::registerEvent('onVoteUpdated', 'skills_onVoteUpdated');

Aseco::addChatCommand(ChatCommands::ELO['full'], ChatCommands::ELO['description']);
Aseco::addChatCommand(ChatCommands::TRUESKILL['full'], ChatCommands::TRUESKILL['description']);
Aseco::addChatCommand(ChatCommands::NEWSEASON['full'], ChatCommands::NEWSEASON['description']);
Aseco::addChatCommand(ChatCommands::SEASON['full'], ChatCommands::SEASON['description']);

global $skills;

abstract class ChatCommands {
    const ELO = array( 'full' => 'elo', 'short' => 'se', 'description' => 'Shows your current ELO' );
    const TRUESKILL = array( 'full' => 'trueskill', 'short' => 'sts', 'description' => 'Shows your current TrueSkill rating' );
    const NEWSEASON = array( 'full' => 'newseason', 'short' => 'sns', 'description' => 'Starts a new season' );
    const SEASON = array( 'full' => 'season', 'short' => 'ss', 'description' => 'Shows chosen season infos' );
}

abstract class RatingType {
    const ELO = 'Elo';
    const TRUESKILL = 'TrueSkill';
}

class RatingRules {
    const MINIMUM_PLAYER_COUNT = 2;

    public static function AssertRaceValidity ( array $rankings ) {
        //
        $player_count_is_greater_or_equal = false;
        {
            $player_count_is_greater_or_equal = count( $rankings ) >= RatingRules::MINIMUM_PLAYER_COUNT;
        }
        //
        $at_least_one_player_with_valid_score = false;
        {
            // check for at least one player with score > 0
            foreach( $rankings as $ranking ) {
                if ( $ranking['Score'] > 0 ) {
                    $at_least_one_player_with_valid_score = true;
                    break;
                }
            }
        }
        //
        return $player_count_is_greater_or_equal && $at_least_one_player_with_valid_score;
    }
}

abstract class VoteType {
    const CHALLENGE_RESTART = 'ChallengeRestart';
    const NEXT_CHALLENGE = 'NextChallenge';
}

abstract class VoteState {
    const NEW_VOTE = 'NewVote';
    const VOTE_PASSED = 'VotePassed';
    const VOTE_FAILED = 'VoteFailed';
}

class Logger {
    public static function debug( string $module, int $line, string $text ) {
        global $aseco;
        $aseco->console( sprintf( '[%s] [%s@%d] - %s', 'Skills', $module, $line, $text ) );
    }

    public static function fatal( $errno, $errstr, $errfile, $errline ) {
        global $aseco;
        $aseco->console_text( sprintf( '[%s] [%s@%d] - %s ', $errno, $errfile, $errline, $errstr ) );
    }
}

abstract class SkillsDbBase {
    abstract function getRatingType();

    public function checkTables() {
        Logger::debug( __METHOD__, __LINE__, $this->getRatingType() .' - checking/creating tables' );

        $query = 'CREATE TABLE IF NOT EXISTS `seasons` (
                 `Id` INT(11) NOT NULL AUTO_INCREMENT, 
                 `Name` VARCHAR(50) NOT NULL,
                 `StartDate` DATETIME NOT NULL DEFAULT \'1970-01-01 00:00:00\',
                 `EndDate` DATETIME NULL,
                 `RatingType` VARCHAR(50) NOT NULL DEFAULT \''. RatingType::ELO .'\',
                 PRIMARY KEY (`id`),
                 UNIQUE INDEX `id_UNIQUE` (`id` ASC),
                 UNIQUE INDEX `name_UNIQUE` (`name` ASC),
                 UNIQUE INDEX `startdate_UNIQUE` (`startdate` ASC)
                 ) ENGINE=MyISAM;';

        mysql_query($query);

        // update logic
        $query = 'SELECT RatingType FROM `seasons`';

        $res = mysql_query($query);

        if ( !$res )  {
            $query = 'ALTER TABLE `seasons` 
                     ADD COLUMN `RatingType` VARCHAR(50) NOT NULL DEFAULT \'Elo\' AFTER `EndDate`;';

            mysql_query( $query );
        }
    } // checkTables

    protected function truncateTable ( string $table ) {
        $query = 'TRUNCATE TABLE '. $table;
        $res = mysql_query( $query );
        if(!$res) {
            Logger::debug( __METHOD__, __LINE__, 'Failed to delete all records from '. $table .' table => '. mysql_error() );
        }
    }


    abstract public function isPlayerIndexed( int $playerId );

    abstract public function addNewPlayer( int $playerId );

    abstract public function updatePlayerRating( int $playerId, array $rating );

    abstract public function getPlayerRating( int $playerId );

    abstract public function getPlayerStanding( int $playerId );

    public function getPlayerInfo( int $id ) {
        $player_info = array();
        //
        $query = 'SELECT * FROM players WHERE `Id`=' . strval($id) . ';';
        $res = mysql_query($query);
        if($res) {
            $player_info = mysql_fetch_array($res);
        }
        //
        return $player_info;
    } // getPlayerInfo

    public function initializeSeasons( ) {
        $query = 'SELECT count(*) FROM seasons;';
        $res = mysql_query($query);
        if ($res) {
            $row = mysql_fetch_row($res);
            $seasons_count = intval($row[0]);
            if ( $seasons_count == 0 ) { # first season
                Logger::debug( __METHOD__, __LINE__, $this->getRatingType() .' - No seasons found - will initialize first season' );
                $this->startNewSeason();
            }
        } else {
            Logger::debug( __METHOD__, __LINE__, $this->getRatingType() .' - Failed to execute statement \''. $query .'\' => '. mysql_error() );
        }
    } // initializeSeasons

    public function startNewSeason( ) {
        $current_season = $this->getCurrentSeason();
        $current_season_id = intval($current_season['id']);
        $new_season_name = sprintf('Season %d', $current_season_id + 1 /* next season */);
        $rating_type = $this->getRatingType();
        // insert new season 
        $query = 'INSERT INTO seasons (`Name`,`StartDate`,`RatingType`) VALUES (\''. $new_season_name .'\', now(), \''. $rating_type .'\');';
        $res = mysql_query($query);
        if(!$res) {
            Logger::debug( __METHOD__, __LINE__, $this->getRatingType() .' - Failed to find add '. $new_season_name .' => '. mysql_error() );
        }
    } // startNewSeason

    abstract public function finishCurrentSeason(); 

    public function getSeason( int $id ) {
        $season_name = '';
        $season_start_date = '';
        $season_end_date = '';
        $rating_type = '';
        //
        $query = 'SELECT * FROM seasons WHERE `Id`='. strval($id) .';';
        $res = mysql_query($query);
        if ( $res ) {
            if (mysql_num_rows($res) > 0) {
                $row = mysql_fetch_row($res);
                $season_name = $row[1];
                $season_start_date = $row[2];
                $season_end_date = $row[3];
                $rating_type = $row[4];
            } else {
                Logger::debug( __METHOD__, __LINE__, $this->getRatingType() .' - Failed to find season id '. strval($id) );
            }
        }
        return array(
            'id' => $id,
            'name' => $season_name,
            'start_date' => $season_start_date,
            'end_date' => $season_end_date,
            'rating_type' => $rating_type
        );
    } // getSeason

    public function getCurrentSeason() {
        $season_id = '0'; // only useful on first season setup
        $season_name = '';
        $season_start_date = '';
        $season_end_date = '';
        $rating_type = '';
        // get last seasons id - if any
        $query = 'SELECT * FROM seasons WHERE `Id`=(SELECT max(`Id`) FROM seasons);';
        $res = mysql_query($query);
        if($res) {
            if (mysql_num_rows($res) > 0) {
                $row = mysql_fetch_row($res);
                $season_id = $row[0];
                $season_name = $row[1];
                $season_start_date = $row[2];
                $season_end_date = $row[3];
                $rating_type = $row[4];
            } else {
                Logger::debug( __METHOD__, __LINE__, $this->getRatingType() .' - Failed to find latest season id' );
            }
        } else {
            Logger::debug( __METHOD__, __LINE__, $this->getRatingType() .' - Failed to find latest season id => '. mysql_error() );
        }
        return array(
            'id' => $season_id,
            'name' => $season_name,
            'start_date' => $season_start_date,
            'end_date' => $season_end_date,
            'rating_type' => $rating_type
        );
    } // getCurrentSeason

    abstract public function getSeasonStandings( int $id );

    abstract public function getCurrentSeasonStandings();

    public function getSeasonsList() {
        $seasons_list = array();
        //
        $query = 'SELECT * FROM seasons';
        $res = mysql_query($query);
        while ($row = mysql_fetch_array($res, MYSQL_ASSOC)) {
            array_push( $seasons_list, $row );
        };
        //
        return $seasons_list;
    }

    abstract public function eraseRecordsForCurrentSeason();
}

class SkillsDbElo extends SkillsDbBase {
    private const BASE_ELO_RATING = 1500;

    public function getRatingType() {
        return RatingType::ELO;
    }

    public function checkTables() {
        parent::checkTables();
        
        $query = 'CREATE TABLE IF NOT EXISTS `elo` (
                 `Id` INT NOT NULL AUTO_INCREMENT,
                 `PlayerId` MEDIUMINT(9) NOT NULL,
                 `Elo` INT NOT NULL DEFAULT '. SkillsDbElo::BASE_ELO_RATING. ',
                 PRIMARY KEY (`Id`),
                 UNIQUE INDEX `PlayerId_UNIQUE` (`PlayerId` ASC)
                 ) ENGINE=MyISAM;';

        mysql_query($query);

        $query = 'CREATE TABLE IF NOT EXISTS `seasons_elo_results` (
                 `EloPlayerId` MEDIUMINT(9) NOT NULL,
                 `SeasonId` INT(11) NOT NULL,
                 `Elo` INT(11) NOT NULL,
                 UNIQUE KEY `EloPlayerIdSeasonId_UNIQUE` (`EloPlayerId`,`SeasonId`)
                 ) ENGINE=MyISAM;';

        mysql_query($query);
    }

    public function isPlayerIndexed( int $playerId ) {
        $query = 'SELECT * FROM elo WHERE `PlayerId` = '. $playerId .';';
        $res = mysql_query($query);
        if($res) {
            if (mysql_num_rows($res) > 0) {
                $playerExist = true;
            }
        } else {
            Logger::debug( __METHOD__, __LINE__, 'Failed to find tracked player id '. $playerId .' => '. mysql_error() );
            $playerExist = false;
        }
        mysql_free_result($res);
        return $playerExist;
    } 

    public function addNewPlayer( int $playerId ) {
        $query = 'INSERT INTO elo (`PlayerId`,`Elo`) VALUES ('. $playerId .', '. SkillsDbElo::BASE_ELO_RATING .' );';
        $res = mysql_query($query);
        if(!$res) {
            Logger::debug( __METHOD__, __LINE__, 'Failed to add player id '. $playerId .' => '. mysql_error() );
        }
    } // addNewPlayer

    public function updatePlayerRating( int $playerId, array $rating ) {
        $elo = $rating['elo'];
        $query = 'UPDATE elo SET `Elo` = '. $elo .' WHERE `PlayerId` = '. $playerId .';';
        $res = mysql_query($query);
        if(!$res) {
            Logger::debug( __METHOD__, __LINE__, 'Failed to update player id '. $playerId .' => '. mysql_error() );
        }
    } // updatePlayerRating

    public function getPlayerRating( int $playerId  ) {
        $query = 'SELECT `Elo` FROM elo WHERE `PlayerId` =  '. $playerId .';';
        $res = mysql_query($query);
        $elo = SkillsDbElo::BASE_ELO_RATING;
        if($res) {
            if (mysql_num_rows($res) > 0) {
                $row = mysql_fetch_row($res);
                $elo = $row[0];
            } else {
                
                Logger::debug( __METHOD__, __LINE__, 'Failed to find player id '. $playerId .' => '. mysql_error() );
            }
        } else {
            Logger::debug( __METHOD__, __LINE__, '[ELO] Failed to get player id '. $playerId .' elo => '. mysql_error() );
        }
        mysql_free_result($res);
        return array( 'elo' => $elo );
    } // getPlayerRating

    function getPlayerStanding( int $playerId ) {
        $query = 'SELECT * FROM elo ORDER BY `Elo` DESC;';
        $res = mysql_query( $query );
        $standing = 1;
        $num_players = 1;
        if($res) {
            $num_players = mysql_num_rows($res);
            if ( $num_players > 0) {
                while ($row = mysql_fetch_array($res)) {
                    if( intval($row['PlayerId']) == $playerId ) {
                        Logger::debug( __METHOD__, __LINE__, 'Current player standing is '. $standing );
                        break;
                    }
                    $standing++;
                }
            } else {
                Logger::debug( __METHOD__, __LINE__, 'Failed to find any record' );
            }
        } else {
            Logger::debug( __METHOD__, __LINE__, 'Failed to find any record  => '. mysql_error() );
        }
        // 
        return array(
            'standing' => $standing, 
            'num_players' => $num_players
        );
    } // getPlayerStanding

    public function getCurrentSeasonStandings() {
        $standings = array();
        // assuming current season standings are in elo table
        $query = 'SELECT * FROM elo ORDER BY `Elo` DESC;';
        $res = mysql_query($query);
        $standing = 1;
        while ($row = mysql_fetch_array($res, MYSQL_ASSOC)) {
            $player_info = $this->getPlayerInfo( $row['PlayerId'] );
            $login = $player_info['login'];
            array_push($standings, array(
                'player_info' => $player_info,
                'standing' => $standing,
                'elo' => $row['Elo']
            ));
            $standing++;
        }
        return $standings;
    } // getCurrentSeasonStandings

    public function getSeasonStandings( int $id ) {
        $standings = array();
        $query = 'SELECT * FROM seasons_elo_results WHERE `SeasonId`=' . strval($id) . ' ORDER BY `Elo` DESC;';
        $res = mysql_query($query);
        $standing = 1;
        while ($row = mysql_fetch_array($res, MYSQL_ASSOC)) {
            $player_info = $this->getPlayerInfo( $row['EloPlayerId'] );
            $login = $player_info['login'];
            array_push($standings, array(
                'player_info' => $player_info,
                'standing' => $standing,
                'elo' => $row['Elo']
            ));
            $standing++;
        }
        return $standings;
    } // getSeasonStandings

    public function finishCurrentSeason() { 
        $current_season = $this->getCurrentSeason();
        // save current players' elo in season_elo_results
        $query = 'SELECT * FROM elo;';
        $res = mysql_query($query);
        while ($row = mysql_fetch_array($res, MYSQL_ASSOC)) {
            $query = 'INSERT INTO seasons_elo_results (`SeasonId`,`EloPlayerId`,`Elo`) VALUES (' . strval($current_season['id']) . ',' . strval($row['PlayerId']) . ',' . strval($row['Elo']) . ');';
            $res1 = mysql_query($query);
            if(!$res1) {
                Logger::debug( __METHOD__, __LINE__, 'Failed to add player id '. $row['PlayerId'] .' standing to seasons_elo_results => '. mysql_error() );
                return;
            }
        }

        // update EndDate for current season
        $query = 'UPDATE seasons SET `EndDate`=now() WHERE `Id`='. strval($current_season['id']) .';';
        $res = mysql_query($query);
        if(!$res) {
            Logger::debug( __METHOD__, __LINE__, 'Failed to update season id '. strval($current_season['id']) .' end date => '. mysql_error() );
            return;
        }

        // drop current elo standings
        $this->truncateTable('elo');
    } // finishCurrentSeason

    public function eraseRecordsForCurrentSeason() {
        $this->truncateTable('elo');
    }
}

class SkillsDbTrueSkill extends SkillsDbBase {
    const DEFAULT_MEAN = 25.0;
    const DEFAULT_STANDARD_DEVIATION = 8.333333333333333333333333333333;

    public function getRatingType() {
        return RatingType::TRUESKILL;
    }

    public function checkTables() {
        parent::checkTables();

        $query = 'CREATE TABLE IF NOT EXISTS `trueskill` (
                 `Id` INT NOT NULL AUTO_INCREMENT,
                 `PlayerId` MEDIUMINT(9) NOT NULL,
                 `Mean` DOUBLE(10,4) NOT NULL DEFAULT '. SkillsDbTrueSkill::DEFAULT_MEAN .',
                 `StdDev` Double(31,30) NOT NULL DEFAULT '. SkillsDbTrueSkill::DEFAULT_STANDARD_DEVIATION .',
                 PRIMARY KEY (`Id`),
                 UNIQUE INDEX `PlayerId_UNIQUE` (`PlayerId` ASC)
                 ) ENGINE=MyISAM;';

        mysql_query($query);

        $query = 'CREATE TABLE IF NOT EXISTS `seasons_trueskill_results` (
                 `TrueSkillPlayerId` MEDIUMINT(9) NOT NULL,
                 `SeasonId` INT(11) NOT NULL,
                 `Mean` DOUBLE(10,4) NOT NULL,
                 `StdDev` Double(31,30) NOT NULL,
                 UNIQUE KEY `TrueSkillPlayerIdSeasonId_UNIQUE` (`TrueSkillPlayerId`,`SeasonId`)
                 ) ENGINE=MyISAM;';

        mysql_query($query);
    }

    public function isPlayerIndexed( int $playerId ) {
        $query = 'SELECT * FROM trueskill WHERE `PlayerId` = '. $playerId .';';
        $res = mysql_query($query);
        if($res) {
            if (mysql_num_rows($res) > 0) {
                $playerExist = true;
            }
        } else {
            Logger::debug( __METHOD__, __LINE__, 'Failed to find tracked player id '. $playerId .' => '. mysql_error() );
            $playerExist = false;
        }
        mysql_free_result($res);
        return $playerExist;
    } // isPlayerIndexed

    public function addNewPlayer( int $playerId ) {
        $query = 'INSERT INTO trueskill (`PlayerId`,`Mean`,`StdDev`) VALUES ('. $playerId .', '. SkillsDbTrueSkill::DEFAULT_MEAN .', '. SkillsDbTrueSkill::DEFAULT_STANDARD_DEVIATION .' );';
        $res = mysql_query($query);
        if(!$res) {
            Logger::debug( __METHOD__, __LINE__, 'Failed to add player id '. $playerId .' => '. mysql_error() );
        }
    } // addNewPlayer

    function getPlayerRating( int $playerId ) {
        $mean = SkillsDbTrueSkill::DEFAULT_MEAN;
        $std_dev = SkillsDbTrueSkill::DEFAULT_STANDARD_DEVIATION;
        $query = 'SELECT `Mean`, `StdDev` FROM trueskill WHERE `PlayerId` =  '. $playerId .';';
        $res = mysql_query($query);
        if ( $res ) {
            if ( mysql_num_rows( $res ) > 0 ) {
                $row = mysql_fetch_row( $res );
                $mean = $row[0];
                $stdDev = $row[1];
            } else {
                Logger::debug( __METHOD__, __LINE__, 'Failed to find player id '. $playerId );
            }
        } else {
            Logger::debug( __METHOD__, __LINE__, 'Failed to get player id '. $playerId .' TrueSkill => '. mysql_error()  );
        }
        mysql_free_result($res);
        return array(
            'mean' => $mean, 
            'std_dev' => $stdDev
        );
    } // getPlayerRating

    function updatePlayerRating( int $playerId, array $rating ) {
        $query = 'UPDATE trueskill SET `Mean` = '. $rating['mean'] .', `StdDev` = '. $rating['std_dev'] .' WHERE `PlayerId` = '. $playerId .';';
        $res = mysql_query($query);
        if(!$res) {
            Logger::debug( __METHOD__, __LINE__, 'Failed to update player id '. $playerId .' TrueSkill => '. mysql_error()  );
        }
    } // updatePlayerRating

    function getPlayerStanding( int $playerId ) {
        $query = 'SELECT * FROM trueskill ORDER BY `Mean` DESC;';
        $res = mysql_query( $query );
        $standing = 1;
        $num_players = 1;
        if($res) {
            $num_players = mysql_num_rows($res);
            if ( $num_players > 0) {
                while ($row = mysql_fetch_array($res)) {
                    if( intval($row['PlayerId']) == $playerId ) {
                        Logger::debug( __METHOD__, __LINE__, 'Current player standing is '. $standing );
                        break;
                    }
                    $standing++;
                }
            } else {
                Logger::debug( __METHOD__, __LINE__, 'Failed to find any record ' );
            }
        } else {
            Logger::debug( __METHOD__, __LINE__, 'Failed to find record '. mysql_error()  );
        }
        return array(
            'standing' => $standing, 
            'num_players' => $num_players
        );
    } // getPlayerStanding

    public function getCurrentSeasonStandings() {
        $standings = array();
        // assuming current season standings are in elo table
        $query = 'SELECT * FROM trueskill ORDER BY `Mean` DESC;';
        $res = mysql_query($query);
        $standing = 1;
        while ($row = mysql_fetch_array($res, MYSQL_ASSOC)) {
            $player_info = $this->getPlayerInfo( $row['PlayerId'] );
            $login = $player_info['login'];
            array_push($standings, array(
                'player_info' => $player_info,
                'standing' => $standing,
                'mean' => $row['Mean'],
                'std_dev' => $row['StdDev']
            ));
            $standing++;
        }
        return $standings;
    }

    public function getSeasonStandings( int $id ) { 
        $standings = array();
        $query = 'SELECT * FROM seasons_trueskill_results WHERE `SeasonId`=' . strval($id) . ' ORDER BY `Mean` DESC;';
        $res = mysql_query($query);
        $standing = 1;
        while ($row = mysql_fetch_array($res, MYSQL_ASSOC)) {
            $player_info = $this->getPlayerInfo( $row['TrueSkillPlayerId'] );
            $login = $player_info['login'];
            array_push($standings, array(
                'player_info' => $player_info,
                'standing' => $standing,
                'mean' => $row['Mean'],
                'std_dev' => $row['StdDev']
            ));
            $standing++;
        }
        return $standings;
    }

    public function finishCurrentSeason() { 
        $current_season = $this->getCurrentSeason();
        // save current players' elo in season_elo_results
        $query = 'SELECT * FROM trueskill;';
        $res = mysql_query($query);
        while ($row = mysql_fetch_array($res, MYSQL_ASSOC)) {
            $query = 'INSERT INTO seasons_trueskill_results (`SeasonId`,`TrueSkillPlayerId`,`Mean`,`StdDev`) 
                      VALUES (' . strval($current_season['id']) . ',' . strval($row['PlayerId']) . ',' . strval($row['Mean']) . ','. strval($row['StdDev']) .');';
            $res1 = mysql_query($query);
            if ( !$res1 ) {
                Logger::debug( __METHOD__, __LINE__, 'Failed to add player id '. $row['PlayerId'] .' seasons_trueskill_results to seasons_elo_results => '. mysql_error() );
                return;
            }
        }

        // update EndDate for current season
        $query = 'UPDATE seasons SET `EndDate`=now() WHERE `Id`='. strval($current_season['id']) .';';
        $res = mysql_query($query);
        if ( !$res ) {
            Logger::debug( __METHOD__, __LINE__, 'Failed to update season id '. strval($current_season['id']) .' end date => '. mysql_error() );
            return;
        }

        // drop current trueskill standings
        $this->truncateTable('trueskill');
    }

    public function eraseRecordsForCurrentSeason() {
        $this->truncateTable('trueskill');
    }
}

class SkillsCore {
    
    const XASECO_MIN_VERSION = '1.14';
    const XASECO_SUPPORTED_GAMES = array('TMF');
    const SEASON_EXPIRATION_DAYS = 14; // days
    const SEASON_EXPIRATION_CHECK_INTERVAL = 300; // seconds - equals to 5 minutes
    const MAIN_DB = RatingType::TRUESKILL;

    private $aseco; // pointer to global $aseco object - to avoid declaring "global $aseco" in all the methods
    private $dbs; // helper methods for database access
    private $gui; // helper class for gui related stuff
    private $players; // array that contains the players present at the start of a challenge

    public function __construct( $aseco ) {
        $this->aseco = $aseco;
        $this->gui = new SkillsGui( $aseco );
        // database instantiation
        $this->dbs = array( 
            RatingType::ELO => new SkillsDbElo(), 
            RatingType::TRUESKILL => new SkillsDbTrueSkill() 
        );
        //
        $this->throwAwaySnapshottedPlayers();
    }

    public function onSync( ) {
        // Check for the right XAseco-Version
        if ( defined('XASECO_VERSION') ) {
            if ( version_compare(XASECO_VERSION, SkillsCore::XASECO_MIN_VERSION, '<') ) {
                trigger_error('['. __FILE__ .'] XAseco version is not supported ('. XASECO_VERSION .')! Please update to min. version '. SkillsCore::XASECO_MIN_VERSION .'!', E_USER_ERROR);
            }
        } else {
            trigger_error('['. __FILE__ .'] Can not identify the system, "XASECO_VERSION" is unset! This plugin runs only with XAseco/'. SkillsCore::XASECO_MIN_VERSION .'+', E_USER_ERROR);
        }

        // Make sure the Server is a supported one.
        if ( !in_array($this->aseco->server->getGame(), SkillsCore::XASECO_SUPPORTED_GAMES) ) {
            trigger_error('['. __FILE__ .'] This plugin supports only the following games '. print_r( SkillsCore::XASECO_SUPPORTED_GAMES, true ) .', cannot start with a "'. $this->aseco->server->getGame() .'" Dedicated-Server!', E_USER_ERROR);
        }
        
        foreach( $this->dbs as $db ) {
            $db->checkTables();
        }
        $this->dbs[SkillsCore::MAIN_DB]->initializeSeasons();

        Logger::debug( __METHOD__, __LINE__, 'Started' );
    }

    public function onEverySecond( ) {
        $t = time();
        if ( $t % SkillsCore::SEASON_EXPIRATION_CHECK_INTERVAL /* seconds */ == 0 ) { 
            $current_season = $this->dbs[SkillsCore::MAIN_DB]->getCurrentSeason();
            $start_date = new DateTime($current_season['start_date']);
            $current_date = new DateTime();
            $interval = $current_date->diff($start_date, true);
            $days = intval($interval->days);
            if ( $days >= SkillsCore::SEASON_EXPIRATION_DAYS ) {
                Logger::debug( __METHOD__, __LINE__, $current_season['name'] .' is expired...' );
                // season expired - finish current season and start a new one
                $current_season = $this->dbs[SkillsCore::MAIN_DB]->getCurrentSeason();
                if ( $current_season['rating_type'] != SkillsCore::MAIN_DB ) {
                    // the rating changed - finish season with that rating and start a new season with the new rating
                    Logger::debug( __METHOD__, __LINE__, 'last season had '. $current_season['rating_type'] .' new one has '. SkillsCore::MAIN_DB );
                    $this->dbs[$current_season['rating_type']]->finishCurrentSeason();
                    // just to make sure we aren't in mixed situations - such as implementing a new rating system whilst on another main one.
                    $this->dbs[SkillsCore::MAIN_DB]->eraseRecordsForCurrentSeason();
                } else { // rating type hasn't changed - finish season normally
                    $this->dbs[SkillsCore::MAIN_DB]->finishCurrentSeason();
                }
                $this->dbs[SkillsCore::MAIN_DB]->startNewSeason();
            }
        }
    }

    public function onPlayerConnect( $player ) {
        Logger::debug( __METHOD__, __LINE__, $player->login .' (id='. $player->id .') connected...' );
        $this->indexPlayerOnDbs( $player->id );
        //
        foreach ( $this->dbs as $db ) {
            $placement = $db->getPlayerStanding( $player->id );
            $rating = $db->getPlayerRating( $player->id );
            $info_for_gui = array(
                'player' => $player,
                'placement' => $placement,
                'season_placements' => $db->getCurrentSeasonStandings( ),
                'rating' => $rating
            );
            //
            $this->gui->showChatRating( $db->getRatingType(), $info_for_gui );
            if ( $db->getRatingType() == SkillsCore::MAIN_DB ) {
                $this->gui->showCurrentSeasonPermanentWidget( $info_for_gui );
            }
        }

        // check if there's no snapshotted players - this should occur only on server restarts and no players are present
        $this->snapshotPlayersIfNone();
    }

    public function onNewChallenge( $info ) {
        // snapshot the current connected players to avoid situations like a player connecting at the end of a round.
        // the player array will have to be resetted on each endRace and snapshotted again, or if a vote is cast
        if ( $this->snapshotPlayers() == 0 ) {
            // we will have to snapshot again once at least a player connects and begins the race
            Logger::debug( __METHOD__, __LINE__, 'No players connected at the start of a new challenge - will snapshot when a new round begins' );
        }
    }

    public function onBeginRound() {
        // check if there's no snapshotted players - this should occur only on server restarts and no players are present
        $this->snapshotPlayersIfNone();
    }

    public function onEndRace( $info ) { 
        $rankings = $this->diffAgainstPlayersSnaphot( $info[0] );
        if ( RatingRules::AssertRaceValidity( $rankings ) ) {
            $this->updateRatings( $rankings );
            $this->throwAwaySnapshottedPlayers();
        } else {
            Logger::debug ( __METHOD__, __LINE__, 'Invalid race - won\'t award any rating' );
        }
    }

    public function onChatCommand( $command, $info ) {
        switch( $command ) {
            case ChatCommands::ELO['full']:
                $player = $info['author'];
                $placement = $this->dbs[RatingType::ELO]->getPlayerStanding( $player->id );
                $rating = $this->dbs[RatingType::ELO]->getPlayerRating( $player->id );
                //
                $info_for_gui = array(
                    'player' => $player,
                    'placement' => $placement,
                    'rating' => $rating
                );
                //
                $this->gui->showChatRating( RatingType::ELO, $info_for_gui );
                break;
            case ChatCommands::TRUESKILL['full']:
                $player = $info['author'];
                $placement = $this->dbs[RatingType::TRUESKILL]->getPlayerStanding( $player->id );
                $rating = $this->dbs[RatingType::TRUESKILL]->getPlayerRating( $player->id );
                //
                $info_for_gui = array(
                    'player' => $player,
                    'placement' => $placement,
                    'rating' => $rating
                );
                //
                $this->gui->showChatRating( RatingType::TRUESKILL, $info_for_gui );
                break;
            case ChatCommands::NEWSEASON['full']:
                $player = $info['author'];
                if ( $this->aseco->isMasterAdmin( $player ) ) {
                    // season expired - finish current season and start a new one
                    $current_season = $this->dbs[SkillsCore::MAIN_DB]->getCurrentSeason();
                    $this->gui->showChatFinishingSeason( $info, $current_season );
                    if ( $current_season['rating_type'] != SkillsCore::MAIN_DB ) {
                        // the rating changed - finish season with that rating and start a new season with the new rating
                        Logger::debug( __METHOD__, __LINE__, 'last season had '. $current_season['rating_type'] .' new one has '. SkillsCore::MAIN_DB );
                        $this->dbs[$current_season['rating_type']]->finishCurrentSeason();
                        // just to make sure we aren't in mixed situations - such as implementing a new rating system whilst on another main one.
                        $this->dbs[SkillsCore::MAIN_DB]->eraseRecordsForCurrentSeason();
                    } else { // rating type hasn't changed - finish season normally
                        $this->dbs[SkillsCore::MAIN_DB]->finishCurrentSeason();
                    }
                    //
                    unset( $current_season );
                    $this->dbs[SkillsCore::MAIN_DB]->startNewSeason();
                    $current_season = $this->dbs[SkillsCore::MAIN_DB]->getCurrentSeason();
                    $this->gui->showChatStartingNewSeason( $info, $current_season );
                    // also re-index every player
                    
                    foreach ( $this->aseco->server->players->player_list as $player ) {
                        $this->indexPlayerOnDbs( $player->id );
                    }
                    // and show them their new standings
                    $info_for_gui = array(
                        'season_placements' => $this->dbs[SkillsCore::MAIN_DB]->getCurrentSeasonStandings()
                    );
                    $this->gui->showCurrentSeasonPermanentWidget( $info_for_gui );
                } else {
                    Logger::debug( __METHOD__, __LINE__, sprintf('id=%d does not have the proper permission to start a new season', $player->id) );
                }
                break;
            case ChatCommands::SEASON['full']:
                $info['params'] = explode(' ', $info['params']);
                $info_for_gui = array(); // will be initialized in the following statements
                if ( count($info['params']) > 0 && $info['params'][0] != '' ) {
                    if ( $info['params'][0] == 'list' ) { // season list asked
                        $info_for_gui = array(
                            'player' => $info['author'],
                            'seasons_list' => $this->dbs[SkillsCore::MAIN_DB]->getSeasonsList()
                        );
                        Logger::debug( __METHOD__, __LINE__, print_r( $info_for_gui['seasons_list'], true ) );
                        $this->gui->showSeasonsList( $info_for_gui );
                    } else if ( (int)$info['params'][0] > 0 ) { // specific season asked
                        $season_id = intval($info['params'][0]);
                        //
                        $chosen_season = $this->dbs[SkillsCore::MAIN_DB]->getSeason( $season_id );
                        $season_placements = $this->dbs[$chosen_season['rating_type']]->getSeasonStandings( $season_id );
                        //
                        $info_for_gui = array(
                            'player' => $info['author'],
                            'chosen_season' => $chosen_season,
                            'season_placements' => $season_placements
                        );
                        $this->gui->showChatChosenSeason( $chosen_season['rating_type'], $info_for_gui );
                        $this->gui->showSeasonWindow( $info_for_gui );
                    }
                } else { // no params -> current season
                    //
                    $chosen_season = $this->dbs[SkillsCore::MAIN_DB]->getCurrentSeason();
                    $season_placements = $this->dbs[SkillsCore::MAIN_DB]->getCurrentSeasonStandings();
                    //
                    $info_for_gui = array(
                        'player' => $info['author'],
                        'chosen_season' => $chosen_season,
                        'season_placements' => $season_placements
                    );
                    $this->gui->showChatChosenSeason( SkillsCore::MAIN_DB, $info_for_gui );
                    $this->gui->showSeasonWindow( $info_for_gui );
                }
                
                break;
            default:
                Logger::debug( __METHOD__, __LINE__, 'unmanaged chat command => '. $command );
                break;
        }
    }

    public function onPlayerManialinkPageAnswer( array $answer ) {
        $info = null;
        $requested_info = $this->gui->needInfoForPageAnswer( $answer );
        //
        if ( $requested_info['type'] == InfoType::CURRENT_SEASON_INFO ) {
            Logger::debug( __METHOD__, __LINE__, 'Retrieving current season infos since answer is '. $answer[2] );
            $info = array(
                'season_placements' => $this->dbs[SkillsCore::MAIN_DB]->getCurrentSeasonStandings(),
                'chosen_season' => $this->dbs[SkillsCore::MAIN_DB]->getCurrentSeason()
            );
        } else if ( $requested_info['type'] == InfoType::CHOSEN_SEASON_INFO ) {
            Logger::debug( __METHOD__, __LINE__, 'Retrieving chosen season ('. $requested_info['data']['season_id'] .') infos since answer is '. $answer[2] );
            $data = $requested_info['data'];
            $chosen_season = $this->dbs[SkillsCore::MAIN_DB]->getSeason( $data['season_id'] );
            $season_placements = $this->dbs[$chosen_season['rating_type']]->getSeasonStandings( $data['season_id'] );
            $info = array(
                'chosen_season' => $chosen_season,
                'season_placements' => $season_placements
            );
        }
        //
        return $this->gui->onPlayerManialinkPageAnswer( $answer, $info );
    }

    public function onVoteUpdated( array $info ) {
        // this method will decide if the Race (comprised of more Rounds) is valid or not
        $vote = $info[2];
        $state = $info[0];
        switch( $state ) {
            case VoteState::VOTE_PASSED:
                Logger::debug ( __METHOD__, __LINE__, 'Vote '. $vote .' passed' );
                if ( $vote == VoteType::CHALLENGE_RESTART ) {
                    // todo: eventually move this to the proper event -> onRestartChallenge( $aseco, $challenge )
                    Logger::debug( __METHOD__, __LINE__, 'Snapshot again' );
                    $this->snapshotPlayers(); // here snapshotPlayers is called because we want to refresh the snapshotted players
                } else {
                    // reset $players array
                    Logger::debug( __METHOD__, __LINE__, 'Will not award any rating change' );
                    $this->throwAwaySnapshottedPlayers();
                }
                break;
            default:
                // unhandled vote state - do nothing
                break;
        }
    }

    private function updateRatings( array $rankings )
    {
        $this->calculateAndUpdateELO( $rankings );
        $this->calculateAndUpdateTrueSkill( $rankings );
        $season_placements = $this->dbs[SkillsCore::MAIN_DB]->getCurrentSeasonStandings( );
        $info_for_gui = array(
            'season_placements' => $season_placements
        );
        // show the new rankings based on the calculations above - for all logins
        $this->gui->showCurrentSeasonPermanentWidget( $info_for_gui );
    }

    private function calculateAndUpdateELO( array $rankings ) {
        try {
            $m = new ELOMatch;
            $pids = array();
            $logins = array();
            
            foreach( $rankings as $ranking ) {
                $playerId = $this->aseco->getPlayerId( $ranking['Login'] );
                $m->addPlayer( strval($playerId), $ranking['Rank'], $this->dbs[RatingType::ELO]->getPlayerRating( $playerId )['elo'] );
                array_push( $pids, $playerId );
                array_push( $logins, $ranking['Login'] );
                Logger::debug( __METHOD__, __LINE__, $ranking['Login'] .' => Rank '. $ranking['Rank'] );
            }

            $m->calculateELOs();

            foreach ($pids as $pid) {
                $newELO = $m->getELO( $pid );
                Logger::debug( __METHOD__, __LINE__, 'id='. $pid .' new ELO is '. $newELO );
                $this->dbs[RatingType::ELO]->updatePlayerRating( $pid, array( 'elo' => $newELO ) );
            }
        } catch (Exception $e) {
            Logger::debug( __METHOD__, __LINE__, 'exception: '. $e->getMessage() );
        }
    }

    private function calculateAndUpdateTrueSkill( $rankings ) {
        try {
            $calculator = new FactorGraphTrueSkillCalculator();
            //
            $teams = array();
            $teamRanks = array();
            $teamScore = array();
            //
            $gameInfo = new GameInfo();
            // it's assumed that the rankings array is sorted from first to last place
            foreach ( $rankings as $ranking ) {
                //
                $playerId = $this->aseco->getPlayerId( $ranking['Login'] );
                Logger::debug( __METHOD__, __LINE__, 'ranking - player='. $ranking['Login'] .', id='. $playerId );
                //
                $player = new Player( $playerId );
                // retrieve rating from database
                $dbRating = $this->dbs[RatingType::TRUESKILL]->getPlayerRating( $playerId );
                $rating = new Rating( $dbRating['mean'], $dbRating['std_dev'] );
                Logger::debug( __METHOD__, __LINE__, 'id='. $player->getId() .' current rating => '. $rating );
                // add player inside the team
                $team = new Team( $player, $rating );
                // take in consideration the previous player's rank - this will define a draw
                if ( $ranking['Score'] == end($teamScore)  ) { // draw with the previous player
                    array_push( $teamRanks, end($teamRanks) ); // same rank as previous player
                } else {
                    array_push( $teamRanks, $ranking['Rank'] ); // different rank
                }
                // go on and add the player object to the Teams array
                array_push( $teams, $team );
                // and add his score to the teamScores array - for future comparison
                array_push( $teamScore, $ranking['Score'] );
            }
            //
            RankSorter::sort($teams, $teamRanks);
            //
            Logger::debug( __METHOD__, __LINE__, 'calculating new rating for every player...' );
            $ratings = $calculator->calculateNewRatings( $gameInfo, $teams, $teamRanks );
            Logger::debug( __METHOD__, __LINE__, 'ratings calculated' );
            //
            foreach ( $teams as $team ) {
                $players = $team->getAllPlayers();
                foreach ( $players as $player ) { // should be only one (FFA)
                    $newRating = $ratings->getRating( $player );
                    Logger::debug( __METHOD__, __LINE__, 'id='. $player->getId() .' new rating => '. $newRating );
                    $this->dbs[RatingType::TRUESKILL]->updatePlayerRating( $player->getId(), array('mean' => $newRating->getMean(), 'std_dev' => $newRating->getStandardDeviation() ) );
                }
            }
        } catch (Exception $e) {
            Logger::debug( __METHOD__, __LINE__, 'Exception: '. $e->getMessage() );
        }
    }

    private function snapshotPlayersIfNone() {
        // this method differs from snapshotPlayers since it will check if $this->players is populated before snapshotting the current players
        if ( count( $this->players ) == 0 ) {
            Logger::debug ( __METHOD__, __LINE__, 'No player snapshotted - go on' );
            $this->snapshotPlayers();
        } else {
            Logger::debug ( __METHOD__, __LINE__, 'There are some snapshotted players ('. count($this->players) .') - won\'t do anything' );
        }
    }

    private function snapshotPlayers () {
        Logger::debug ( __METHOD__, __LINE__, 'Snapshotting current players:' );
        $this->players = $this->aseco->server->players->player_list;
        foreach ( $this->players as $player ) {
            Logger::debug ( __METHOD__, __LINE__, 'id ['. $player->id .'] pid ['. $player->pid .'] - '.$player->login );
        }
        return count($this->players);
    }

    private function diffAgainstPlayersSnaphot ( array $rankings ) {
        $diff = array();
        foreach ( $rankings as $ranking ) {
            $login = $ranking['Login'];
            Logger::debug ( __METHOD__, __LINE__, 'Checking if ['. $login .'] is part of the snapshot' );
            foreach ( $this->players as $player ) { // I know it's bad - will optimize it in the future, if possible
                if ( $player->login == $login ) { // player present in the snapshot - add it
                    Logger::debug( __METHOD__, __LINE__, 'Adding ['. $login .'] as it was present at the start of the challenge' );
                    array_push( $diff, $ranking );
                }
            }
        }
        return $diff;
    }

    private function throwAwaySnapshottedPlayers() {
        Logger::debug ( __METHOD__, __LINE__, 'Throwing away the snapshotted players' );
        $this->players = array();
    }

    private function indexPlayerOnDbs( int $player_id ) {
        // cycle through each database
        foreach( $this->dbs as $db ) {
            if ( !$db->isPlayerIndexed( $player_id ) ) {
                $db->addNewPlayer( $player_id );
                Logger::debug( __METHOD__, __LINE__, 'Player (id='. $player_id .') is being tracked on RatingType='. $db->getRatingType() .'.'  );
            }
        }
    }
};

abstract class InfoType {
    const NO_INFO_NEEDED = 'None';
    const CURRENT_SEASON_INFO = 'CurrentSeason';
    const CHOSEN_SEASON_INFO = 'ChosenSeason';
}

class SkillsGui {
    const BASE_MANIALINK_ID = '911';

    // windows id
    const CHOSEN_SEASON_MAINWINDOW_MANIALINK_ID = SkillsGui::BASE_MANIALINK_ID . '00';
    const CHOSEN_SEASON_SUB_WINDOW_MANIALINK_ID = SkillsGui::BASE_MANIALINK_ID . '01';
    const CURRENT_SEASON_PERMANENT_MANIALINK_ID = SkillsGui::BASE_MANIALINK_ID . '13';
    const SEASONS_LIST_MANIALINK_ID = SkillsGui::BASE_MANIALINK_ID . '14';
    // actions id
    const CHOSEN_SEASON_CLOSE_BUTTON_ACTION_ID = SkillsGui::BASE_MANIALINK_ID . '05';
    const CURRENT_SEASON_PERMANENT_ACTION_ID = SkillsGui::BASE_MANIALINK_ID . '06';
    const SEASONS_LIST_CLOSE_BUTTON_ACTION_ID = SkillsGui::BASE_MANIALINK_ID . '19';
    const SEASONS_LIST_BASE_ACTION_ID = SkillsGui::BASE_MANIALINK_ID . '20';
    private $seasons_list_action_ids;

    private $aseco;

    public function __construct( $aseco ) {
        $this->aseco = $aseco;
        $this->seasons_list_action_ids = array();
    }

    // chat

    // let the user know their current ELO - info should have the necessary infos
    public function showChatRating( string $rating_type, array $info ) {
        switch( $rating_type ) {
            case RatingType::ELO:
                $this->showChatRatingElo( $info );
                break;
            case RatingType::TRUESKILL:
                $this->showChatRatingTrueSkill( $info );
                break;
            default:
                Logger::debug( __METHOD__, __LINE__, 'unhandled rating_type => '. $rating_type );
                break;
        }
    }

    private function showChatRatingElo( array $info ) {
        try {
            // extract data from $info array
            $player = $info['player'];
            $placement = $info['placement'];
            $rating = $info['rating'];
            // compose the chat message
            $message = sprintf( '{#server}>> {#message}Your current ELO rating is {#highlite}[%d] {#rank}(%d/%d)', $rating['elo'], $placement['standing'], $placement['num_players'] );
            $message = $this->aseco->formatColors( $message );
            // send the chat message
            $this->aseco->client->query( 'ChatSendServerMessageToLogin', $message, $player->login );
        } catch ( Exception $e ) {
            Logger::debug( __METHOD__, __LINE__, 'Exception: '. $e->getMessage() );
        }
    }

    private function showChatRatingTrueSkill( array $info ) {
        try {
            // extract data from $info array
            $player = $info['player'];
            $placement = $info['placement'];
            $rating = $info['rating'];
            // compose the chat message
            $message = sprintf( '{#server}>> {#message}Your current TrueSkill rating is {#highlite}[%.2f] (stdDev=%.2f) {#rank}(%d/%d)', $rating['mean'], $rating['std_dev'], $placement['standing'], $placement['num_players'] );
            $message = $this->aseco->formatColors( $message );
            // send the chat message
            $this->aseco->client->query( 'ChatSendServerMessageToLogin', $message, $player->login );
        } catch ( Exception $e ) {
            Logger::debug( __METHOD__, __LINE__, 'Exception: '. $e->getMessage() );
        }
    }

    public function showChatChosenSeason( string $rating_type, array $info ) {
        switch( $rating_type ) {
            case RatingType::ELO:
                $this->showChatChosenSeasonElo( $info );
                break;
            case RatingType::TRUESKILL:
                $this->showChatChosenSeasonTrueSkill( $info );
                break;
            default:
                Logger::debug( __METHOD__, __LINE__, 'unhandled rating_type '. $rating_type );
                break;
        }
    }

    private function showChatChosenSeasonElo( array $info ) {
        // let the user know about the chosen season
        try {
            $player = $info['player'];
            $chosen_season = $info['chosen_season'];
            $season_placements = $info['season_placements'];
            //
            $season_standings = null;
            $end_date = 'ongoing';
            if ( isset($chosen_season['end_date'] ) ) { 
                $end_date = $chosen_season['end_date'];
            }
            // season header
            $message = sprintf( '{#server}>> {#message}Info for {#highlite}%s {#message}(from: {#highlite}%s {#message}to: {#highlite}%s{#message}):', $chosen_season['name'], $chosen_season['start_date'], $end_date );
            $this->aseco->client->query('ChatSendServerMessageToLogin', $this->aseco->formatColors($message), $player->login );
            // cycle through each 
            foreach ( $season_placements as $player_placement ) {
                $standing = $player_placement['standing'];
                $elo = $player_placement['elo'];
                $message = sprintf( '{#server}>>> {#rank}%02d. {#message}%s {#highlite}[%d]', intval($standing), $player_placement['player_info']['NickName'], intval($elo) );
                $this->aseco->client->query('ChatSendServerMessageToLogin', $this->aseco->formatColors($message), $player->login );
            }
        } catch ( Exception $e ) {
            Logger::debug( __METHOD__, __LINE__, 'Exception: '. $e->getMessage() );
        }
    }

    private function showChatChosenSeasonTrueSkill( array $info ) {
        try {
            $player = $info['player'];
            $chosen_season = $info['chosen_season'];
            $season_placements = $info['season_placements'];
            //
            $season_standings = null;
            $end_date = 'ongoing';
            if ( isset($chosen_season['end_date'] ) ) { 
                $end_date = $chosen_season['end_date'];
            }
            // season header
            $message = sprintf( '{#server}>> {#message}Info for {#highlite}%s {#message}(from: {#highlite}%s {#message}to: {#highlite}%s{#message}):', $chosen_season['name'], $chosen_season['start_date'], $end_date );
            $this->aseco->client->query('ChatSendServerMessageToLogin', $this->aseco->formatColors($message), $player->login );
            // cycle through each 
            foreach ( $season_placements as $player_placement ) {
                $standing = $player_placement['standing'];
                $mean = $player_placement['mean'];
                $std_dev = $player_placement['std_dev'];
                $message = sprintf( '{#server}>>> {#rank}%02d. {#message}%s {#highlite}[%.2f] (stdDev=%.2f)', intval($standing), $player_placement['player_info']['NickName'], $mean, $std_dev );
                $this->aseco->client->query('ChatSendServerMessageToLogin', $this->aseco->formatColors($message), $player->login );
            }
        } catch ( Exception $e ) {
            Logger::debug( __METHOD__, __LINE__, 'Exception: '. $e->getMessage() );
        }
    }

    public function showChatStartingNewSeason( array $info, array $season ) {
        $message = sprintf('{#server}>> {#highlite}%s {#message}started!', $season['name']);
        $this->aseco->client->query('ChatSendServerMessage', $this->aseco->formatColors($message));
    }

    public function showChatFinishingSeason( array $info, array $season ) {
        $player = $info['author'];
        //
        $this->aseco->client->query( 'ChatSendServerMessageToLogin', 'Finishing current season and starting a new one...', $player->login );
        $message = sprintf('{#server}>> {#message}Type {#highlite}/season %d {#message}to see last season standings', intval($season['id']));
        $this->aseco->client->query( 'ChatSendServerMessage', $this->aseco->formatColors($message) );
    }

    // gui
    public function needInfoForPageAnswer ( $answer ) {
        if ( $answer[2] == (int)SkillsGui::CURRENT_SEASON_PERMANENT_ACTION_ID ) {
             // refresh current season permanent widget and show a window with current season rankings
            return array(
                'type' => InfoType::CURRENT_SEASON_INFO,
            );
        } else if ( in_array($answer[2], $this->seasons_list_action_ids) ) { // show chosen season rankings
            $season_id = $answer[2] - (int)SkillsGui::SEASONS_LIST_BASE_ACTION_ID;
            return array(
                'type' => InfoType::CHOSEN_SEASON_INFO,
                'data' => array(
                    'season_id' => $season_id
                )
            );
        }
        return array(
            'type' => InfoType::NO_INFO_NEEDED
        );
    }

    public function onPlayerManialinkPageAnswer( $answer, $info = null ) {
        // $answer = [0]=PlayerUid, [1]=Login, [2]=Answer
        if ( $answer[2] == 0 ) {  // If id = 0, bail out immediately
            return;
        }
        // Init
        $widgets = '';
        // Get the Player object
        $player = $this->aseco->server->players->player_list[$answer[1]];
        //
        if ( $answer[2] == (int)SkillsGui::CHOSEN_SEASON_CLOSE_BUTTON_ACTION_ID ) {
            // Close Window
            $widgets .= $this->closeChosenSeasonWindow();
        } else if ( $answer[2] == (int)SkillsGui::CURRENT_SEASON_PERMANENT_ACTION_ID ) { // clicked current seasons standing widget
            if ( !is_null($info) ) {
                $season_placements = $info['season_placements'];
                $widgets .= $this->buildCurrentSeasonStandingsWidget( $season_placements ); // refresh
                $widgets .= $this->buildSeasonStandingsWindow( $info ); // and show big window
            }
        } else if ( $answer[2] == (int)SkillsGui::SEASONS_LIST_CLOSE_BUTTON_ACTION_ID ) {
            $widgets .= $this->closeSeasonsListWindow(); // close seasons list window 
        } else if ( in_array( $answer[2], $this->seasons_list_action_ids ) ) {
            Logger::debug( __METHOD__, __LINE__, 'Selected a season' );
            $widgets .= $this->closeSeasonsListWindow(); // close seasons list window
            if ( !is_null($info) ) {
                $season_placements = $info['season_placements'];
                $widgets .= $this->buildSeasonStandingsWindow( $info ); // and show big window
            }
        }
        // Send all widgets
        if ($widgets != '') {
            // Send Manialink
            $this->sendManialink( $widgets, $player->login, 0 /* timeout */ );
        }
    }

    private function sendManiaLink( $widgets, $login = false, $timeout = 0 ) {
        $xml  = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<manialinks>';
        $xml .= $widgets;
        $xml .= '</manialinks>';

        if ( $login != false ) {
            // Send to specific Player
            Logger::debug ( __METHOD__, __LINE__, 'Sending manialink to specific login => '. $login );
            $this->aseco->client->query( 'SendDisplayManialinkPageToLogin', $login, $xml, ($timeout * 1000), false );
        } else {
            // Send to all connected Players
            Logger::debug ( __METHOD__, __LINE__, 'Sending manialink to all connected players' );
            $this->aseco->client->query( 'SendDisplayManialinkPage', $xml, ($timeout * 1000), false );
        }
    }

    public function buildWindowWidgetBody( string $manialink_id, string $close_button_action_id ) {
        //--------------------------------------------------------------//
        // BEGIN: Window                                                //
        //--------------------------------------------------------------//
        // %icon_style%, %icon_substyle%
        // %window_title%
        // %prev_next_buttons%
        $header .= '<manialink id="'. $manialink_id .'">';
        $header .= '<frame posn="-10.1 30.45 18.50">';    // BEGIN: Window Frame
        $header .= '<quad posn="0.8 -0.8 0.01" sizen="21.4 53.7" bgcolor="001B"/>';
        $header .= '<quad posn="-0.2 0.2 0.04" sizen="23.4 55.7" style="Bgs1InRace" substyle="BgWindow2"/>';

        // Header Line
        $header .= '<quad posn="0.8 -1.3 0.02" sizen="21.4 3" bgcolor="09FC"/>';
        $header .= '<quad posn="0.8 -4.3 0.03" sizen="21.4 0.1" bgcolor="FFF9"/>';
        $header .= '<quad posn="1.8 -1 0.04" sizen="3.2 3.2" style="%icon_style%" substyle="%icon_substyle%"/>';

        // Title
        $header .= '<label posn="5.5 -1.9 0.04" sizen="20 0" textsize="2" scale="0.9" textcolor="FFFF" text="%window_title%"/>';

        // Close Button
        $header .= '<frame posn="19.4 1.3 0.05">';
        $header .= '<quad posn="0 0 0.01" sizen="4 4" style="Icons64x64_1" substyle="ArrowDown"/>';
        $header .= '<quad posn="1.1 -1.35 0.02" sizen="1.8 1.75" bgcolor="EEEF"/>';
        $header .= '<quad posn="0.65 -0.7 0.03" sizen="2.6 2.6" action="'. $close_button_action_id .'" style="Icons64x64_1" substyle="Close"/>';
        $header .= '</frame>';

        $header .= '%prev_next_buttons%';

        // Footer
        $footer  = '</frame>';              // END: Window Frame
        $footer .= '</manialink>';

        return array( 
            'header' => $header,
            'footer' => $footer
        );
    }

    public function closeChosenSeasonWindow() {
        $xml  = '<manialink id="'. SkillsGui::CHOSEN_SEASON_MAINWINDOW_MANIALINK_ID .'"></manialink>';  // MainWindow
        return $xml;
    }

    public function closeSeasonsListWindow() {
        $xml = '<manialink id="'. SkillsGui::SEASONS_LIST_MANIALINK_ID .'"></manialink>';  // MainWindow
        return $xml;
    }

    private function getRatingFromItem( array $item ) {
        $rating = '';
        if ( array_key_exists('elo', $item) ) {
            $rating = $item['elo'];
        } else if ( array_key_exists('mean', $item) ) {
            $rating = sprintf('%.2f', $item['mean']);
        }
        return $rating;
    }

    public function buildCurrentSeasonStandingsWidget( array $season_placements, int $limit = 12, int $topcount = 3 ) {
        // Add Widget header
        $build = $this->buildCurrentSeasonStandingsPermanentWidgetBody();
        $xml = $build['header'];
        if ( count($season_placements) > 0) {
            // Create the Widget entries
            $rank = 1;
            $line = 0;
            foreach ( $season_placements as &$item ) {
                // get the player rating
                
                $tmp = array(
                    'rank' => $item['standing'],
                    'score' => $this->getRatingFromItem($item),
                    'nickname' => $item['player_info']['NickName']
                );
                // Build record entries
                $xml .= $this->getCloseToYouEntry( $tmp, $line, $topcount, $placeholder, 15.5 );
                //
                $line ++;
                $rank ++;
                //
                if ( $line >= $limit ) {
                    break;
                }
            }
            unset($item);
        }

        // Add Widget footer
        $xml .= $build['footer'];

        // Send the Widget now
        return $xml;
    } // buildCurrentSeasonStandingsWidget

    private function getCloseToYouEntry ( $item, $line, $topcount, $widgetwidth, $nicemode = true ) {
        // Set offset for calculation the line-heights
        $offset = 3;
        // Set default Text color
        $textcolor = 'DDDF';
        // Do not build the Player related highlites if in NiceMode!
        $xml = '';
        //
        if ($nicemode == false) {
            $textcolor = '3F5F';
            //
            if ( (isset($item['highlitefull'])) && ($item['highlitefull']) ) {
                // Add only a Background for this Player with an record here, if it is in $topcount
                $xml .= '<quad posn="0.4 -'. (1.8 * $line + $offset - 0.3) .' 0.003" sizen="'. ($widgetwidth - 0.8) .' 2" style="BgsPlayerCard" substyle="BgCardSystem"/>';
            }
            if ($item['rank'] != false) {
                // $item['rank'] is set 'false' in Team to skip the highlite here in re_buildLiveRankingsWidget()
                $xml .= '<quad posn="-1.9 -'. (1.8 * $line + $offset - 0.3) .' 0.003" sizen="2 2" style="BgsPlayerCard" substyle="BgCardSystem"/>';
                $xml .= '<quad posn="-1.7 -'. (1.8 * $line + $offset - 0.1) .' 0.004" sizen="1.6 1.6" style="Icons64x64_1" substyle="ArrowNext"/>';
                $xml .= '<quad posn="'. ($widgetwidth - 0.1) .' -'. (1.8 * $line + $offset - 0.3) .' 0.003" sizen="2 2" style="BgsPlayerCard" substyle="BgCardSystem"/>';
                $xml .= '<quad posn="'. ($widgetwidth + 0.1) .' -'. (1.8 * $line + $offset - 0.1) .' 0.004" sizen="1.6 1.6" style="Icons64x64_1" substyle="ArrowPrev"/>';
            }
        }
        //
        if ($item['rank'] != false) {
            if ( ($nicemode == true) && ($item['rank'] <= $topcount) ) {
                $textcolor = 'FF0F';
            } else if ( ($nicemode == true) && ($item['rank'] > $topcount) ) {
                $textcolor = '19FF';
            }
            //
            $xml .= '<label posn="2.3 -'. (1.8 * $line + $offset) .' 0.004" sizen="1.7 1.7" halign="right" scale="0.9" text="'. $item['rank'] .'."/>';
            $xml .= '<label posn="5.9 -'. (1.8 * $line + $offset) .' 0.004" sizen="3.8 1.7" halign="right" scale="0.9" textcolor="'. $textcolor .'" text="'. $item['score'] .'"/>';
        }
        $xml .= '<label posn="6.1 -'. (1.8 * $line + $offset) .' 0.004" sizen="'. sprintf("%.02f", ($widgetwidth - 5.7)) .' 1.7" scale="0.9" text="'. $item['nickname'] .'"/>';
        //
        return $xml;
    } // getCloseToYouEntry

    private function buildSeasonStandingsWindow( $info ) {
        $build = $this->buildWindowWidgetBody( SkillsGui::CHOSEN_SEASON_MAINWINDOW_MANIALINK_ID, SkillsGui::CHOSEN_SEASON_CLOSE_BUTTON_ACTION_ID );
        //
        $season_standings = $info['season_placements'];
        $season = $info['chosen_season'];
        //
        if ( count($season_standings) > 0 ) {
            // Add all connected PlayerLogins
            $players = array();
            foreach ($this->aseco->server->players->player_list as &$player) {
                $players[] = $player->login;
            }
            unset($player);

            $xml = str_replace(
                array(
                    '%icon_style%',
                    '%icon_substyle%',
                    '%window_title%',
                    '%prev_next_buttons%'
                ),
                array(
                    'BgRaceScore2',
                    'ScoreLink',
                    $season['name'],
                    ''
                ),
                $build['header']
            );

            $xml .= '<frame posn="2.5 -6.5 1">';
            $xml .= '<format textsize="1" textcolor="FFFF" />';

            $xml .= '<quad posn="0 0.8 0.02" sizen="17.75 46.88" style="BgsPlayerCard" substyle="BgCard"/>';

            $rank = 1;
            $line = 0; 
            $offset = 0;
            foreach ($season_standings as &$item) {
                // Mark current connected Players
                if ( in_array($item['player_info']['login'], $players) ) {
                    $xml .= '<quad posn="'. ($offset + 0.4) .' '. (((1.83 * $line - 0.2) > 0) ? -(1.83 * $line - 0.2) : 0.2) .' 0.03" sizen="16.95 1.83" style="BgsPlayerCard" substyle="BgCardSystem"/>';
                }
                $xml .= '<label posn="'. (2.6 + $offset) .' -'. (1.83 * $line) .' 0.04" sizen="2 1.7" halign="right" scale="0.9" text="'. $rank .'."/>';
                $xml .= '<label posn="'. (6.4 + $offset) .' -'. (1.83 * $line) .' 0.04" sizen="4 1.7" halign="right" scale="0.9" textcolor="DDDF" text="'. $this->getRatingFromItem($item) .'"/>';
                $xml .= '<label posn="'. (6.9 + $offset) .' -'. (1.83 * $line) .' 0.04" sizen="11.2 1.7" scale="0.9" text="'. $item['player_info']['NickName'] .'"/>';

                $line ++;
                $rank ++;

                if ($line % 25 == 0) {
                    $offset += 19.05;
                }

                // Display max. 25 entries, count start from 1
                if ($rank > 25) {
                    break;
                }
            }
            unset($item);
            $xml .= '</frame>';
            $xml .= $build['footer'];
            return $xml;
        }
    } // buildSeasonStandingsWindow

    private function buildCurrentSeasonStandingsPermanentWidgetBody () {
        //--------------------------------------------------------------//
        // BEGIN: RecordWidgets (Dedimania, Ultimania, Locals, Live)    //
        //--------------------------------------------------------------//
        // %manialinkid%
        // %actionid%
        // %posx%, %posy%
        // %widgetwidth%, %widgetheight%
        // %column_width_name%, %column_height%
        // %title_background_width%
        // %image_open_pos_x%, %image_open_pos_y%, %image_open%
        // %posx_icon%, %posy_icon%, %icon_style%, %icon_substyle%
        // %posx_title%, %posy_title%
        // %halign%, %title%
        $header  = '<manialink id="%manialinkid%">';
        $header .= '<frame posn="%posx% %posy% 0">';

        $header .= '<quad posn="0 0 0.001" sizen="%widgetwidth% %widgetheight%" action="%actionid%" style="Bgs1InRace" substyle="NavButton"/>';
        $header .= '<quad posn="0.4 -2.6 0.002" sizen="2 %column_height%" bgcolor="AAA5"/>';
        $header .= '<quad posn="2.4 -2.6 0.002" sizen="3.65 %column_height%" bgcolor="AAA3"/>';
        $header .= '<quad posn="6.05 -2.6 0.002" sizen="%column_width_name% %column_height%" bgcolor="AAA1"/>';
        $header .= '<quad posn="%image_open_pos_x% %image_open_pos_y% 0.05" sizen="3.5 3.5" image="%image_open%"/>';

        // Icon and Title
        $header .= '<quad posn="0.4 -0.36 0.002" sizen="%title_background_width% 2" style="BgsPlayerCard" substyle="BgRacePlayerName"/>';
        $header .= '<quad posn="%posx_icon% %posy_icon% 0.004" sizen="2.5 2.5" style="%icon_style%" substyle="%icon_substyle%"/>';
        $header .= '<label posn="%posx_title% %posy_title% 0.004" sizen="10.2 0" halign="%halign%" textsize="1" text="%title%"/>';
        $header .= '<format textsize="1" textcolor="FFFF"/>';

        $footer  = '</frame>';
        $footer .= '</manialink>';

        $build = array(
            'header' => $header,
            'footer' => $footer
        );

        $positions = array(
            'left'  => array(
                'icon' => array(
                    'x'      => 0.6,
                    'y'      => 0
                ),
                'title' => array(
                    'x'      => 3.2,
                    'y'      => -0.55,
                    'halign' => 'left'
                ),
                'image_open' => array(
                    'x'      => -0.3,
                    'image'  => 'http://maniacdn.net/undef.de/xaseco1/records-eyepiece/edge-open-ld-dark.png'
                )
            ),
            'right' => array(
                'icon' => array(
                    'x'      => 12.5,
                    'y'      => 0
                ),
                'title' => array(
                    'x'      => 12.4,
                    'y'      => -0.55,
                    'halign' => 'right'
                ),
                'image_open' => array(
                    'x'      => 12.2,
                    'image'  => 'http://maniacdn.net/undef.de/xaseco1/records-eyepiece/edge-open-rd-dark.png'
                )
            )
        );


        // Set the right Icon and Title position
        $position = 'left';

        // Set the Topcount
        $topcount = 3;

        // Calculate the widget height (+ 3.3 for title)
        $widget_height = ( 1.8 * 12 + 3.3);

        if ($position == 'right') {
            $imagex = ($positions[$position]['image_open']['x'] + (15.5 - 15.5));
            $iconx  = ($positions[$position]['icon']['x'] + (15.5 - 15.5));
            $titlex = ($positions[$position]['title']['x'] + (15.5 - 15.5));
        }
        else {
            $imagex = $positions[$position]['image_open']['x'];
            $iconx  = $positions[$position]['icon']['x'];
            $titlex = $positions[$position]['title']['x'];
        }

        $build['header'] = str_replace(
            array(
                '%manialinkid%',
                '%actionid%',
                '%posx%',
                '%posy%',
                '%image_open_pos_x%',
                '%image_open_pos_y%',
                '%image_open%',
                '%posx_icon%',
                '%posy_icon%',
                '%icon_style%',
                '%icon_substyle%',
                '%halign%',
                '%posx_title%',
                '%posy_title%',
                '%widgetwidth%',
                '%widgetheight%',
                '%column_width_name%',
                '%column_height%',
                '%title_background_width%',
                '%title%'
            ),
            array(
                SkillsGui::CURRENT_SEASON_PERMANENT_MANIALINK_ID,
                SkillsGui::CURRENT_SEASON_PERMANENT_ACTION_ID,
                '49.2',
                '-7.7',
                $imagex,
                -($widget_height - 3.3),
                $positions[$position]['image_open']['image'],
                $iconx,
                $positions[$position]['icon']['y'],
                'Icons128x128_1',
                'Rankings',
                $positions[$position]['title']['halign'],
                $titlex,
                $positions[$position]['title']['y'],
                15.5,
                $widget_height,
                (15.5 - 6.45),
                ($widget_height - 3.1),
                (15.5 - 0.8),
                'Current season'
            ),
            $build['header']
        );

        // Add Background for top X Players
        if ( $topcount > 0 ) {
            $build['header'] .= '<quad posn="0.4 -2.6 0.003" sizen="'. (15.5 - 0.8) .' '. (( $topcount * 1.8 ) + 0.3 ) .'" style="BgsPlayerCard" substyle="BgCardSystem"/>';
        }

        return $build;
    } // buildCurrentSeasonStandingsPermanentWidgetBody

    private function buildSeasonsListWindow( array $info ) {
        // get the header and footer of the widget 
        $build = $this->buildWindowWidgetBody( SkillsGui::SEASONS_LIST_MANIALINK_ID, SkillsGui::SEASONS_LIST_CLOSE_BUTTON_ACTION_ID );
        // start building the data
        $seasons_list = $info['seasons_list'];
        if ( count($seasons_list) > 0 ) { // at least one season to list
             $xml = str_replace(
                array(
                    '%icon_style%',
                    '%icon_substyle%',
                    '%window_title%',
                    '%prev_next_buttons%'
                ),
                array(
                    'BgRaceScore2',
                    'ScoreLink',
                    'Seasons',
                    ''
                ),
                $build['header']
            );
            //
            $xml .= '<frame posn="2.5 -6.5 1">';
            $xml .= '<format textsize="1" textcolor="FFFF" />';
            $xml .= '<quad posn="0 0.8 0.02" sizen="17.75 46.88" style="BgsPlayerCard" substyle="BgCard"/>'; // background - whole widget
            //
            $line = 0; 
            $offset = 0; // for future use
            foreach ( $seasons_list as $season ) {
                array_push( $this->seasons_list_action_ids, (int)SkillsGui::SEASONS_LIST_BASE_ACTION_ID + $line + 1 );
                //
                $xml .= '<quad posn="'. ($offset + 0.4) .' '. (((1.83 * $line - 0.2) > 0) ? -(1.83 * $line - 0.2) : 0.2) .' 0.03" action="'. strval(end($this->seasons_list_action_ids)) .'" sizen="16.95 1.83" style="BgsPlayerCard" substyle="BgCardSystem"/>';
                $xml .= '<label posn="'. (6.9 + $offset) .' -'. (1.83 * $line) .' 0.04" sizen="17.2 1.7" scale="0.9" text="'. $season['Name'] .'"/>';
                //
                $line ++;
                // for future use
                /*if ($line % 25 == 0) {
                    $offset += 19.05;
                }*/
            }
            //
            $xml .= '</frame>';
            $xml .= $build['footer'];
            return $xml;
        }
    } // buildSeasonsListWindow

    public function showCurrentSeasonPermanentWidget ( $info ) {
        $season_placements = $info['season_placements'];
        $login = $info['player']->login;
        //
        $xml = $this->buildCurrentSeasonStandingsWidget( $season_placements );
        $this->sendManiaLink( $xml, $login );
    }

    public function showSeasonWindow( $info ) {
        $login = $info['player']->login;
        $xml = $this->buildSeasonStandingsWindow( $info );
        $this->sendManiaLink( $xml, $login );
    }

    public function showSeasonsList( $info ) {
        $login = $info['player']->login;
        $xml = $this->buildSeasonsListWindow( $info );
        $this->sendManiaLink( $xml, $login );
    }
}

function skills_onSync ( $aseco ) {
   global $skills;
   $skills->onSync( );
} // skills_onSync

function skills_onPlayerConnect ($aseco, $player) {
    global $skills;
    $skills->onPlayerConnect( $player );
} // skills_onPlayerConnect

function skills_onNewChallenge( $aseco, $info ) {
    global $skills;
    $skills->onNewChallenge( $info );
}

function skills_onBeginRound( $aseco ) {
    global $skills;
    $skills->onBeginRound();
}

function skills_onEndRace ( $aseco, $info ) {
    global $skills;
    $skills->onEndRace( $info );
} // skills_onEndRace

function skills_onEverySecond ( $aseco ) {
    global $skills;
    $skills->onEverySecond( );
} // skills_onEverySecond

function skills_onPlayerManialinkPageAnswer ($aseco, $answer) {
    global $skills;
    $skills->onPlayerManialinkPageAnswer( $answer );
} // skills_onPlayerManialinkPageAnswer

function skills_onVoteUpdated ( $aseco, $info ) {
    global $skills;
    $skills->onVoteUpdated( $info );
} // skills_onVoteUpdated

function chat_elo ( $aseco, $command ) {
    global $skills;
    $skills->onChatCommand( ChatCommands::ELO['full'], $command );
}  // chat_elo

function chat_trueskill ( $aseco, $command ) {
    global $skills;
    $skills->onChatCommand( ChatCommands::TRUESKILL['full'], $command );
} // chat_trueskill

function chat_newseason ( $aseco, $command ) {
    global $skills;
    $skills->onChatCommand( ChatCommands::NEWSEASON['full'], $command );
} // chat_newseason

function chat_season ( $aseco, $command ) {
    global $skills;
    $skills->onChatCommand( ChatCommands::SEASON['full'], $command );
} // chat_season

register_shutdown_function( 'fatal_handler' );

function fatal_handler( ) {
    $errfile = 'unknown file';
    $errstr  = 'shutdown';
    $errno   = E_CORE_ERROR;
    $errline = 0;

    $error = error_get_last();

    if( $error !== NULL) {
        $errno   = $error['type'];
        $errfile = $error['file'];
        $errline = $error['line'];
        $errstr  = $error['message'];
        
        Logger::fatal ( $errno, $errstr, $errfile, $errline );
    }
}

global $aseco;
$skills = new SkillsCore( $aseco );

?>